# What is PostgreSQL?
PostgreSQL is an open source database management system which supports SQL and JSON. It was created in 1986 by Michael Stonebraker, a computer science professor, and his colleagues. While it is now knows as PostgreSQL, it was originally names Postgres which led to Postgres95 and eventually PostgreSQL in 2009.
### Features 
- Lets administrators create fault-tolerant environments
- Compatible with many operating systems
- Supports multi-version [concurrency control](https://en.wikipedia.org/wiki/Concurrency_control) 
- Follows the ANSI SQL standard
- Object oriented
- Supports geographic objects
- Simple to learn
- Low maintenance
- Open source


## Character Data Types
Character data types store text values. Psql has three character datatypes.

| Name     | Description  |
|----------|--------------|
|varchar(n)|Allows you to declare variable-length with a limit                    |
|Char(n)   |Fixed-length, blank padded                                            |
|Text      |Use can use this data type to declare a variable with unlimited length|

## Numeric Data Types
PostgreSQL can store two types of numbers: integers and floating-point numbers.

| Name      | Store size |  Range |
|-----------|------------|--------|
|smallint| 2 bytes| -32768 to +32767|
|integer| 4 bytes | -2147483648 to +2147483647|
|bigint| 8 bytes | -9223372036854775808 to 9223372036854775807 |
|decimal | variable| If you declared it as decimal datatype ranges from 131072 digits before the decimal point to 16383 digits after the decimal point|
|numeric| variable| If you declare it as the number, you can include number up to 131072 digits before the decimal point to 16383 digits after the decimal point|
|real | 4 bytes| 6 decimal digits precision|
|double| 8 bytes| 15 decimal digits precision|

## Binary Data Types

Binary string:
- Sequence of bytes or octets
- Binary data types in Postgres are divided into two ways
  - Allow the storage of odd or zero values
  - Non-printable octets

| Name | Storage Size                                    | Description                   |
|------|-------------------------------------------------|-------------------------------|
| Byte | 1 to 4 bytes plus the size of the binary string | Variable-length binary string |


## Network Address Type
Postgres has three data types that allow you to store network information.

| Name    | Size          | Description                     |
|---------|---------------|---------------------------------|
| cider   | 7 or 19 bytes | IPV4 and IPv6 networks          |
| Inet    | 7 or 19 bytes | IPV4 and IPV5 host and networks |
| macaddr | 6 bytes       | MAC addresses                   |

Using these data types allows you to save space, check input error, and use functions like searching by subnet.

## Creating Databases
You can simply create a database using 
```SQL
CREATE DATABASE {DBNAME}
-- You can connect to the database using
\c {DBNAME}
-- or
\connect {DBNAME}
```

## Creating Tables

You can create a table using
```SQL
CREATE TABLE {table_name} (
  field_name data_type constraint,
  field_name data_type constraint 
);
-- You can also add IF NOT EXISTS to get a message instead of an error when you try to create a table that already exists

CREATE TABLE IF NOT EXISTS {table_name} (
  field_name data_type constraint,
  field_name data_type constraint 
);

-- Dropping tables is also straightforward
DROP TABLE {table_name};
```

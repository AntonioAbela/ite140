import datetime as dt

dayInput = input('What day?(MM/DD): ')
dayNow = dt.datetime.now()

def daysUntil():
    if int(dayInput[0]) == 0:
        day = dt.datetime(2022, int(dayInput[1]), int(dayInput[3:]))
    else:
        day = dt.datetime(2022, int(dayInput[0:2]), int(dayInput[3:]))
    dayWanted = int(day.strftime('%j'))
    
    print('There are ' + str(dayWanted - int(dayNow.strftime('%j'))) + ' days until ' + str(dayInput))
    
    
    

daysUntil()



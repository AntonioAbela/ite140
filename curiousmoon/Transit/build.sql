CREATE schema if not exists import;
drop table if exists import.master_plan;
CREATE TABLE import.master_plan(
    start_time_utc text,
    duration text,
    date text,
    team text,
    spass_type text,
    target text,
    request_name text,
    library_definition text,
    title text,
    description text
);


COPY master_plan
FROM '../../../cm_data/master_plan.csv'
WITH DELIMITER ',' HEADER CSV;
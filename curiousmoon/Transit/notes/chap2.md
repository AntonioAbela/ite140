# Chapter 2 notes

- `createdb` creates a database
- `dropdb` deletes a database
- Use `\c database` to connect to a database
- You can use `\h` for help with SQL and `\?` for help with psql commands
- Create a table with the create table command. You can choose what variables you can place within the table. Example:
  ```sql
  create table tablename(
    the_date date, 
    title varchar(100), -- varchar is just a string with a maxlength
    description text
  );
  ```
- `drop table if exists tablebame` deletes any table with the same name as the one you are trying to create
- if you set an id a table with `primary serial key` you get an auto incrementing key.
  
# Vocabulary
- ETL: extract, transform, load
- Idempotence
- Schema
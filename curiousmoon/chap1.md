# Summary
The chapter starts off with Dee wondering why their `DBA` is being escorted out of the building. She then gets a message from Rob, the `CTO`, asking to see her. Rob explains that Nas, the former `DBA`, was fired because he dropped the `production cluster`. He further explained that Nas wanted to tweak some things with `AWS` and their account permissions. He set up a `Virtual Private Cloud` for the company. He also put everyone in `Cognito` which is the AWS identity service used for user management. 

When Nas went to update the access key on the production server (red1) after messing with `AWS` he forgot to change his environment variables from when he was on the red2 server. This means that when Nas tried to update the access key it updated it on red2 not red1. When red1 tried to backup using `pg_dumpall` it would fail because it did not have the correct access key. This led to the archive filed not getting deleted and the entire server filling up. 

Nas was in the process of cleaning the both servers and got red1 back up and running, and promoted red2 so he could troubleshoot red1. Nas logged into red2 to clear up as much storage as he could, but when he logged back on to red1 he realized there were no backup archives. What happened was that for the past 3 weeks he forgot to change his environment variables and was on red1 when he thought he was on red2. He ended up deleting all the archives and the data, and because the backups to `AWS` failed, there were no other backups. 

Rob tells Dee that he needs her to act as a temporary `DBA` and introduces the SELFI project. Finally, he assigns Dee to get the data from the link he sent her, load the Master Schedule, normalize it, add constraints, and optimize “stuff.”


## Vocabulary
- DBA: A DBA is a database administrator
- Production Cluster: A production cluster is a cluster of different nodes that share the same cluster name and have a master node which is chosen automatically.
- VPC: A Virtual Private Cloud is a private cloud computing environment that is within a public cloud.
- Cognito: Cognito is an Amazon service which is used for user authentication and management.
- pg_dumpall: pg_dumpall is a utility used for dumping all databases in a cluster into a single script. The script can be used to restore the database.
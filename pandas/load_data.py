import pandas as pd

data = pd.read_excel('pandas/ds_salaries.xlsx')

dataus = data[data['company_location']=='US']
ensal = data[data['experience_level']=='EN']['salary_in_usd']
ensalus = dataus[dataus['experience_level']=='EN']['salary_in_usd']
misal = data[data['experience_level']=='MI']['salary_in_usd']
misalus = dataus[dataus['experience_level']=='MI']['salary_in_usd']
sesal = data[data['experience_level']=='SE']['salary_in_usd']
sesalus = dataus[dataus['experience_level']=='SE']['salary_in_usd']
exsal = data[data['experience_level']=='EX']['salary_in_usd']
exsalus = dataus[dataus['experience_level']=='EX']['salary_in_usd']

print(f'Average Entry-level salary: ${round(ensal.mean())}')
print(f'Average Entry-level salary US: ${round(ensalus.mean())}')
print(f'Average Junior salary: ${round(misal.mean())}')
print(f'Average Junior salary US: ${round(misalus.mean())}')
print(f'Average Senior salary: ${round(sesal.mean())}')
print(f'Average Senior salary US: ${round(sesalus.mean())}')
print(f'Average Expert salary: ${round(exsal.mean())}')
print(f'Average Expert salary US: ${round(exsalus.mean())}')


# work_year: The year the salary was paid.

# experience_level: The experience level in the job during the year with the following possible values: EN Entry-level / Junior MI Mid-level / Intermediate SE Senior-level / Expert EX Executive-level / Director

# employment_type: The type of employement for the role: PT Part-time FT Full-time CT Contract FL Freelance

# job_title: 	The role worked in during the year.

# salary: The total gross salary amount paid.

# salary_currency: The currency of the salary paid as an ISO 4217 currency code.

# salaryinusd: The salary in USD (FX rate divided by avg. USD rate for the respective year via fxdata.foorilla.com).

# employee_residence: Employee's primary country of residence in during the work year as an ISO 3166 country code.

# remote_ratio: The overall amount of work done remotely, possible values are as follows: 0 No remote work (less than 20%) 50 Partially remote 100 Fully remote (more than 80%)

# company_location: The country of the employer's main office or contracting branch as an ISO 3166 country code.

# company_size: he average number of people that worked for the company during the year: S less than 50 employees (small) M 50 to 250 employees (medium) L more than 250 employees (large)


#Found from https://www.kaggle.com/datasets/ruchi798/data-science-job-salaries



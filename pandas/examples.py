import pandas as pd



data = [["Mark", 55, "Italy", 4.5, "Europe"],
        ["John", 33, "USA", 6.7, "America"],
        ["Tim", 41, "USA", 3.9, "America"],
        ["Jenny", 12, "Germany", 9.0, "Europe"]]

df = pd.DataFrame(data=data,
                    columns=["name", "age", "country",
                             "score", "continent"],
                    index=[1001, 1000, 1002, 1003])
# DataFrame:
#         name  age country score continent
# 1001    Mark  55 Italy    4.5   Europe
# 1000    John  33 USA      6.7   America
# 1002    Tim   41 USA      3.9   America
# 1003    Jenny 12 Germany  9.0   Europe

# Data Selection

print(df.loc[1001, 'name']) # Prints Mark

print(df.loc[:1002, ["name", "country"]])
# Prints:
#         name  country
# 1001    Mark  Italy
# 1000    John  USA
# 1002    Tim   USA

print(df['country'] == 'USA')
#Prints:
# 1001    False
# 1000     True
# 1002     True
# 1003    False
# Name: country, dtype: bool

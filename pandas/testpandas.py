import pandas as pd
import matplotlib.pyplot as plt

data = pd.read_excel('pandas/ds_salaries.xlsx')

# Creates pivot table with mean salaries in usd for each experience level at different company sizes
pivot = pd.pivot_table(data,
                 index="experience_level", columns='company_size',
                 values="salary_in_usd",
                 aggfunc='mean')
print(pivot.round().sort_values(by='L'))

# Output:
# company_size             L         M         S
# experience_level                              
# EN                 72813.0   50322.0   62185.0
# MI                 98030.0   90091.0   51159.0
# SE                147591.0  137816.0  116027.0
# EX                221942.0  178242.0  201309.0



# Creates dataframe
df = pd.DataFrame(data)
print(df)
# Prints job title of index 507
print(df.loc[507, 'job_title'])

#Prints job title, salary in usd, and company location for rows 0-10
print(df.loc[:10, ['job_title', 'salary_in_usd', 'company_location']])


# Prints boolean if experience level is equal to SE for each row
print(df['experience_level'] == 'SE')

# Creates new dataframe with only the experience level and salary in usd columns
newdf = df.filter(['experience_level', 'salary_in_usd'])

print(newdf.groupby(['experience_level']).mean())
dfgrouped = newdf.groupby(['experience_level']).mean().sort_values(by=['salary_in_usd'])

fig, ax = plt.subplots()
ax2 = ax.twiny()

pivot.sort_values(by='L').plot(kind = 'bar', ax=ax, color = ['#6B717E', '#155CE0', '#87C2FD']).set_ylim(0,225000)
pivot.sort_values(by='L').plot(kind = 'line', ax=ax, secondary_y=True, ms=10, color = ['#6B717E', '#155CE0', '#87C2FD']).set_ylim(0,225000)
dfgrouped.plot()
plt.show()
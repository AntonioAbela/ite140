print('This program will calculate how many days until a specified date.')
print('Enter the specified date below.')

import datetime as dt

dayWanted = input('What day? (MM/DD/YYYY): ')

today = dt.date.today()
if int(dayWanted[0]) == 0:
    future = dt.date(int(dayWanted[6:]), int(dayWanted[1]),int(dayWanted[3:5]))
else:
    future = dt.date(int(dayWanted[6:]),int(dayWanted[0:2]), int(dayWanted[3:5]))

daysUntil = future - today
print(f'There are {daysUntil.days} days until {dayWanted}')


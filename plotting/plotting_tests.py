import matplotlib.pyplot as plt
import pandas as pd

data = pd.read_csv('plotting/book_dataset.csv')

fig = plt.figure(figsize=(10,10))
ax = fig.add_subplot(projection='3d')
print(data['price'])
ax.scatter(data['pages'],data['avg_reviews'], data['price'])
ax.set_zlabel('Price')
ax.set_xlabel('Pages')
ax.set_ylabel('Average Review')
plt.show()
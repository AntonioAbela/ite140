## Plotting Resources

- ```Matplotlib``` plot types: https://matplotlib.org/stable/plot_types/index
- Plot customization: https://matplotlib.org/stable/tutorials/introductory/customizing.html#sphx-glr-tutorials-introductory-customizing-py
- What charts to use in data visualization: https://chartio.com/learn/charts/how-to-choose-data-visualization/
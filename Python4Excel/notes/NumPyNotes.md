## NumPy Arrays

- A NumPy array is an N-dimensional array for homogenous data
- Commonly dealing with 1 and 2 dimensional arrays using floats

## Vectorization and Broadcasting

- NumPy preforms element-wise operations, this is called vectorization
- If the shape of two arrays are different when using arithmetic operation NumPy will attempt to extent the smaller array
- This is called broadcasting
- Matrix multiplications or dot products are preformed with the @ operator

## Universal Functions (`ufunc`) 

- Universal functions work on every element in the NumPy array.
- NumPy universal functions are faster then Python's standard functions when it comes to NumPy arrays
  
## Creating and Manipulating Arrays

- Chained indexing can give you the n number in n row 

```python
    numpy_array[row_selection, column_selection]
```
- You can create arrays with the `arange` function
- The reshape function generated an array with specified dimensions

```python
    np.arange(2 * 5).reshape(2, 5) # Creates an array with 2 rows and 5 columns
```
- To use the data from an array but not change it you have to copy it
  
```python
    subset = array2[:, :2].copy()
```



# Vocabulary 

#### `Jupyter` Notebooks
- A `Jupyter` Notebook is a computational environment that works with notebook files.
- In a `Jupyter` notebook document code is ordered with numbered input and output fields.
- `Jupyter` notebooks use REPL which is read-eval-print loop. 
  
#### Pandas
- Pandas is a library for Python which is used for data analysis and manipulation.
- Pandas has many features including data structures and operations which are used to manipulate tables and time series.

#### Interface
- An interface can mean a couple things
- It could be a program that allows users to access other computers and devices over the network.
- It could also be a GUI (Graphical User Interface) which gives users control over a program by clicking or modifying visual elements.

#### Data Sets
- A data set is simply a collection of data
- Data sets are generally organized with columns being a variable and each row is a sample of that data.

#### Statistics
- Statistics is all about the collecting, organizing, analyzing, and interpreting of data. 
- Within statistics a statistic is information about a sample and a proportion is information about the population.
- Statistics are used to make predictions about the population.
- Example: A sample is taken of 100 high school students from around Arlington, 80 of them say school lunch is bad. With this information we could assume around 80% of Arlington students think school lunch is bad. When collecting data you also have to make sure that sample sizes are large enough.
- There is a lot more to statistics but those are the basics.

#### Time Series
- A time series is a series of data points which are organized chronologically.
- Usually, data points in a time series are equally spaced apart in time.

#### Vectorization
- Vectorization is the practice of speeding up code by not using loops.
- Vectorization works by working with a set values when computing rather then one at a time.
- ```python
    # Common Vectorization Functions
    outer(a, b): # Compute the outer product of two vectors.
    multiply(a, b): # Matrix product of two arrays.
    dot(a, b): # Dot product of two arrays.
    zeros((n, m)): # Return a matrix of given shape and type, filled with zeros.
    process_time(): # Return the value (in fractional seconds) of the sum of the system and user CPU time of the current process. It does not include time elapsed during sleep.
  ```

#### Data Alignment
- Data alignment is used to force the compiler use to certain data byte boundaries when creating objects.
- This increases efficiency when loading and storing data.

#### Cleaning Data
- Cleaning data is the process of finding and fixing incomplete, inaccurate, or corrupt data from a data table

#### Aggregation 
- Aggregation is the process of turning a set of data into a single value. 
- Example: Data Set: 1,2,5,2,8,3 Mean is an example of aggregation and would be 3.5. More examples of aggregation could be the total, median, min, max, etc...

#### Descriptive Statistics
- Descriptive statistics is a way to interpret a population or sample. Examples of descriptive statistics are mean, median, standard deviation, min, and max. Descriptive statistics tell us about the data set.




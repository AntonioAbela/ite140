import numpy as np
from matplotlib import pyplot as plt
print('This program will create two random arrays, graph them, and find the regression line.')

# Creating random array
x = np.sort(np.random.randint(1,10,10))
y = np.sort(np.random.randint(1,10,10))

# Calculating Regression Line
regline = np.polyfit(x,y,1)
regline_values = [regline[0] * i + regline[1] for i in x]

# Find Summary Stats
x_summary = f'Mean x: {np.mean(x)}, Standard Deviation x: {round(np.std(x),2)}'
y_summary = f'Mean y: {np.mean(y)}, Standard Deviation y: {round(np.std(y), 2)}'
print(x_summary)
print(y_summary)



# Ploting data
plt.bar(x, y, align = 'center')
plt.axline((x[0],y[0]), slope=regline[0], color = 'r',label = 'Regression Line')
plt.title('Bar Graph')
plt.xlim(0,10)
plt.ylim(0,10)
plt.xticks(np.arange(0,10))
plt.ylabel('Y axis')
plt.xlabel('X axis')
plt.legend()
plt.show()







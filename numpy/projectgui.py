import numpy as np
from matplotlib import pyplot as plt
from sklearn.linear_model import LinearRegression
from tkinter import *
import tkinter.font as font

window = Tk()
window.geometry('400x350')
window.title('Super Cool Program')
print('This program creates two arrays with random x and y values. It then graphs it and finds the regression line.')

# Creating random array
x = np.sort(np.random.randint(1,10,10))
y = np.sort(np.random.randint(1,10,10))

# Calculating Regression Line
regline = np.polyfit(x,y,1)
regline_values = [regline[0] * i + regline[1] for i in x]

# Find Summary Stats
x_summary = f'Mean x: {np.mean(x)}, Standard Deviation x: {round(np.std(x),2)}'
y_summary = f'Mean y: {np.mean(y)}, Standard Deviation y: {round(np.std(y), 2)}'
print(x_summary)
print(y_summary)



# Ploting data
plt.bar(x, y, align = 'center')
plt.axline((x[0],y[0]), slope=regline[0], color = 'r',label = 'Regression Line')
plt.title('Bar Graph')
plt.xlim(0,10)
plt.ylim(0,10)
plt.xticks(np.arange(0,10))
plt.ylabel('Y axis')
plt.xlabel('X axis')
plt.legend()

def btnclick():
    plt.show()

btn = Button(window, text='Show Plot', command=btnclick)
btn.config(height=10,
           width=20)
btn['font'] = font.Font(size=30)
btn.grid(column=1, row=0)

window.mainloop()





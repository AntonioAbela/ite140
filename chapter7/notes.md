## Chapter 7
- Using the Path class from `pathlib` allows you to create a path from a string.
- `__file__` is the path of the file
- using `parent` on a path will return the directory the file is in
- `rglob means recursive globbing`
- The `to_excel` method can create an excel file from a DataFrame 
- The `converters` parameter for the `read_excel` method can be used to give columns functions that effect on the data
- 
